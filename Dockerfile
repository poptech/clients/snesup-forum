FROM php:7-apache

RUN a2enmod rewrite

# install the PHP extensions we need
RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev git && rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd && docker-php-ext-install mysqli \
	&& docker-php-ext-install pdo pdo_mysql

ADD . /var/www/html

VOLUME ["/var/www/html/"]

CMD ["apache2-foreground"]
